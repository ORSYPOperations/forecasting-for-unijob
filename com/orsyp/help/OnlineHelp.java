package com.orsyp.help;

public class OnlineHelp {
	public static void displayHelp(int RC, String Message, String Release) {
		
		System.out.println(Message+"\n");
		System.out.println("---------------------------------------------------------------------------");
		System.out.println("      This Tool Generates a Report and a Forecast for existing Jobs        ");
		System.out.println("                         Implemented for HKBN    ");		
		System.out.println("---------------------------------------------------------------------------\n");
		System.out.println("++ Optional Parameters:\n");
		System.out.println("    --login:   UVMS connection login     [can be specified in properties file]");
		System.out.println("    --pwd:     UVMS connection password  [can be specified in properties file]");
		System.out.println("    --uvms:    UVMS hostname             [can be specified in properties file]");
		System.out.println("    --port:    UVMS Connection port      [can be specified in properties file]");
		System.out.println("");
		System.out.println("    --sdate:   START Date for Forecast     [YYYYMMDD]");
		System.out.println("    --stime:   START Time for Forecast     [HHMM]");
		System.out.println("    --edate:   END Date for Forecast       [YYYYMMDD]");
		System.out.println("    --ttime:   END Time for Forecast       [HHMM]");
		System.out.println("");
		System.out.println("++ Display, Help & Version Info:\n");
		System.out.println("    --help:    Display online help");
		System.out.println("    --version: Version Information & UVMS compatibility");
		System.out.println("    --verbose: Display Extra Information");
		System.out.println("");
		System.out.println("  IMPORTANT NOTES:");
		System.out.println("  => !!! IF any of the four parameters edate, etime, sdate or stime is forgotten, only the patterns are displayed !!!");
		System.out.println("  => All Configuration parameters can be modified from properties file in same folder as Jar file");
		System.out.println("");
		System.out.println("++ Examples:\n");
		System.out.println("    UJJobsReport");
		System.out.println("    UJJobsReport --pwd=universe");
		System.out.println("    UJJobsReport --sdate=20121224 --stime=1655 --edate=20121226 etime=2359");
		System.out.println("");
		System.exit(RC);
	}

	public static void displayVersion(int RC, String release, String[] APIversions) {
		System.out.println("Version "+release);
		System.out.println("++ This Tool was compiled and tested with the following UVMS:");
		for (int j=0;j < APIversions.length;j++){
			System.out.println(APIversions[j]);
		}
		System.out.println("  => Other versions of UVMS might not work as expected or might not work at all");
		System.exit(RC);
	}
}
