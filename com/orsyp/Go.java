package com.orsyp;

import java.io.IOException;
import java.util.Properties;
import java.util.regex.Pattern;

import com.orsyp.Area;
import com.orsyp.Environment;
import com.orsyp.Identity;
import com.orsyp.OutcomeReport;
import com.orsyp.SyntaxException;
import com.orsyp.UniverseException;
import com.orsyp.specfilters.JobSpecFilter;
import com.orsyp.specfilters.NodeSpecFilter;
import com.orsyp.specfilters.SpecFilter;
import com.orsyp.specfilters.TargetSpecFilter;
import com.orsyp.specfilters.UserSpecFilter;
import com.orsyp.std.EnvironmentStdImpl;
import com.orsyp.std.central.network.NetworkNodeListStdImpl;
import com.orsyp.unijob.UJJobMgt;
import com.orsyp.unijob.UJJobPackage;
import com.orsyp.unijob.UJPackageClear;
import com.orsyp.unijob.UVMSInterface;
import com.orsyp.util.PropertyLoader;
import com.orsyp.api.Client;
import com.orsyp.api.Context;
import com.orsyp.api.Product;
import com.orsyp.api.central.UniCentral;
import com.orsyp.api.central.UniCentral.LicenseEntryStatus;
import com.orsyp.api.central.networknode.NetworkNodeItem;
import com.orsyp.api.central.networknode.NetworkNodeList;
import com.orsyp.api.central.networknode.NetworkNodeId.PRODUCT_TYPE;
import com.orsyp.help.*;
import edu.mscd.cs.jclo.JCLO;

class AllArgs {  

    private boolean help;
    private String pwd;
    private String login;
    private int port;
    private String uvms;
    private boolean version;
    private String job;
    private String node;
   
    private String target;
    private boolean verbose;
    private String sdate;
    private String stime;
    private String edate;
    private String etime;
    private String user;
}

public class Go {
	
	static String Customer = "HKBN";
	static String Release="2.0";
	static String[] APIversions = {"600"};
	
	private static boolean DEBUGMODE=false;
	
	public static void main(String[] argv) throws IOException, InterruptedException {
		
		// UJ port is retrieved from UVMS once connected
		int UJPort;
		String password="";
		String login="";
		String UVMSHost="";
		int port=0;
		String nodeFilter="";
		String userFilter="";
		String targetFilter="";
		String jobFilter="";
		boolean displayHelp=false;
		boolean displayVer=false;
		boolean verbose=false;
		String STARTDATE="";
		String STARTTIME="";
		String ENDDATE="";
		String ENDTIME="";
		boolean ACTIVE_DATE=true;
		boolean LicExpires=true;
		String type="UNIJOB";
		String USER="";
		
		try {
				System.out.println("=======================================================");
				System.out.println("  ** ORSYP Unijob Job Management Tool version " + Release +"  **  ");
				System.out.println("  *          ORSYP Professional Services.           * ");
				System.out.println("  * Copyright (c) 2014 ORSYP.  All rights reserved. * ");
				System.out.println("  **             Implemented for "+Customer+"              **");
				System.out.println("=======================================================");
				System.out.println("");

				OutcomeReport rep = new OutcomeReport();
				
		        JCLO jclo = new JCLO (new AllArgs());
		        jclo.parse (argv);
		        
		        displayHelp=jclo.getBoolean ("help");
		        displayVer=jclo.getBoolean ("version");
		        password=jclo.getString ("pwd");
		        login=jclo.getString ("login");
		        UVMSHost=jclo.getString ("uvms");
		        
		        port=jclo.getInt ("port");
		        jobFilter=jclo.getString ("job");
		        targetFilter=jclo.getString ("target");
		        nodeFilter=jclo.getString ("node");
		        userFilter=jclo.getString ("user");
		        verbose=jclo.getBoolean ("verbose");
      
		        STARTDATE=jclo.getString ("sdate");
		        STARTTIME=jclo.getString ("stime");
		        ENDDATE=jclo.getString ("edate");
		        ENDTIME=jclo.getString ("etime");
		        
		        checkDateTime(STARTDATE,STARTTIME);
		        checkDateTime(ENDDATE,ENDTIME);
		        
		        if(STARTDATE==null || ENDDATE==null || STARTTIME==null || ENDTIME==null){
		        	ACTIVE_DATE=false;
		        }

		        if(displayHelp){OnlineHelp.displayHelp(0,"Display help",Release);}
		        if(displayVer){OnlineHelp.displayVersion(0,Release,APIversions);}
		        
		        if(verbose){DEBUGMODE=true;}
		        if(!verbose){DEBUGMODE=false;}
		        // TO REMOVE BEFORE DELIVERY
		        // DEBUGMODE=true;
		        Properties pGlobalVariables = PropertyLoader.loadProperties("UJJobsReport");

			    if (password == null || password.equals("") ){password=pGlobalVariables.getProperty("UVMS_PWD");}    
			    if (password.equals("")){OnlineHelp.displayHelp(-1,"Error: No Password Passed!",Release);}
			         
				System.out.println("=> Loading Program Options...");

				// Connection to UVMS
				
	            if (login == null || login.equals("")){login=pGlobalVariables.getProperty("UVMS_USER");}
	            if (UVMSHost == null || UVMSHost.equals("")){UVMSHost=pGlobalVariables.getProperty("UVMS_HOST");}
	            if (port==0){port=Integer.parseInt(pGlobalVariables.getProperty("UVMS_PORT"));}
	            
	            if (jobFilter == null){jobFilter="";}
	            if (targetFilter == null){targetFilter="";}
	            if (nodeFilter == null){nodeFilter="";}
	            if (userFilter == null){userFilter="";}

		        SpecFilter[] allFilters = new SpecFilter[4];
		        NodeSpecFilter nfilter = new NodeSpecFilter(nodeFilter);
		        JobSpecFilter jfilter = new JobSpecFilter(jobFilter);
		        TargetSpecFilter tfilter = new TargetSpecFilter(targetFilter);
		        UserSpecFilter ufilter = new UserSpecFilter(userFilter);
		        
		        allFilters[0]=nfilter;
		        allFilters[1]=jfilter;
		        allFilters[2]=tfilter;
		        allFilters[3]=ufilter;
	    		
	            if(UVMSHost==null||password==null||login==null||UVMSHost==""||port==0||password==""||login==""){
	            	System.out.println("-- Fatal Error in Parameters Passed.");
	            	OnlineHelp.displayHelp(1,"Display help",Release);
	            }
				UVMSInterface uvInterface = new UVMSInterface(UVMSHost,port,login,password,pGlobalVariables.getProperty("DU_NODE"),pGlobalVariables.getProperty("DU_COMPANY"),Area.Exploitation);
				
				UniCentral central = uvInterface.ConnectEnvironment();
				UJPort = Integer.parseInt(central.getCentralConfigurationVariable("DEF_UJ_IO_PORT"));
			
				/**
				 * Retrieve the node list from UVMS, all parameters can be seen below, however we only use some
				 * and we filter on Unijob Nodes only
				 */
				
				boolean checkLicActive=false;
				// Uncomment the following code if you wish to implement a license check
				/*
				 if(checkLicActive){
	            
				
					String LicStr=pGlobalVariables.getProperty("MODULE_LIC");
					boolean RepLicOK=false;
					String LicStat="";
					boolean RepLicFound;
				
					RepLicFound=true;
	
					if (LicStr==null || LicStr.equals("")){RepLicFound=false;System.out.println("  --- ERROR - No Licence for " + Customer + " Module Found.");System.exit(99);}
					else{
						
						if( ! LicStr.equals("7656qlfhgtryidfg12")){
						
						LicenseEntryStatus myLicStat= central.checkLicenseEntry(LicStr);  
						if(myLicStat.toString().equals("VALID")){
							RepLicOK=true;
							// extracting the expiration date in licence line
							String DateExp=LicStr.split(" ")[5].substring(6);
							if (DateExp.equals("20361231")){LicExpires=false;}
								LicStat=myLicStat.toString();
						    }else{
						    	LicStat=myLicStat.toString();
						    }	
						} else {
							RepLicOK=true;
							LicExpires=false;
							LicStat="VALID";
						}
					}
					
					if(!RepLicOK){
						System.out.println("  --- ERROR - Licence for " + Customer + " Module on UVMS has Status: "+LicStat);
						System.exit(99);
					}
				}
				*/
				
				NetworkNodeList networkList = new NetworkNodeList(uvInterface.getContext());
				networkList.setImpl(new NetworkNodeListStdImpl());
				try {
					networkList.extract();
				} catch (UniverseException e1) {
					e1.printStackTrace();
					return;
				}
				for (int i = 0; i < networkList.getCount(); i++) {
					NetworkNodeItem item = networkList.get(i);
					String NodeName=item.getNodeCompanyConfiguration().getNodeName();
					PRODUCT_TYPE ProdType=item.getNodeCompanyConfiguration().getProductType();
					int NodeStat = item.getNodeInfo().getStatus();
				
					if (item.getNodeCompanyConfiguration().getProductType().toString().equals(type) 
							&& nfilter.matchespattern(NodeName)){
					
						System.out.println("=> Processing Node: "+NodeName
						+ ", Type [" + ProdType + "]"
						+ ", OS [" + item.getNodeInfo().getOs() + "]"
						+ ", Status [" + NodeStat +"]");
				
					if (item.getNodeInfo().getStatus() != 1){
						System.out.println("    --- Could not connect to Node "+item.getNodeCompanyConfiguration().getNodeName());
						System.out.println("    --> Skipping Node.");
					} else {
					String group = "*";
					String host = "*";
					String system = "W32";
					Environment environment = null;
					try {
						
						environment = new Environment(PRODUCT_TYPE.UNIJOB.toString(), NodeName);
					} catch (SyntaxException e) {
						e.printStackTrace();
					}
					Client clientUJ = new Client(new Identity(login, group, host, system));
					Context contextUJ = new Context (environment, clientUJ, central);
					contextUJ.setImpl(new EnvironmentStdImpl());
					contextUJ.setProduct(Product.UNIJOB);
					
					UJJobMgt mgt = new UJJobMgt(NodeName, UJPort, uvInterface.getContext(), contextUJ, allFilters, verbose);
					mgt.parseJobs(ACTIVE_DATE,STARTDATE,STARTTIME,ENDDATE,ENDTIME);
					//else{mgt.parseJobs(ACTIVE_DATE);
					}
					}
					else if (item.getNodeCompanyConfiguration().getProductType().toString().equals(type) 
							&& ! nfilter.matchespattern(NodeName) && verbose){
						System.out.println("=> Node "+NodeName+" Skipped.");
					}
				}

				System.out.println("\n=> End of UJ Operations.");
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		//Uncomment following two lines if you want to implement a license check
		//if(LicExpires){System.out.println(" !!! You have no permanent license, program will abort !!!");System.exit(99);}
		//if(!LicExpires){System.exit(0);}
	}

			
public static boolean convertOption(String s){
	if (s.equalsIgnoreCase("O") || s.equalsIgnoreCase("Y")){
		return true;
	}
	return false;
}
public static boolean checkDateTime(String d,String t){
		
	if(d!=null && t!=null){
	 // if(d.equals("")){throw new IllegalArgumentException("Start and End Dates MUST be specified in format YYYYMMDD");}
	 // if(t.equals("")){throw new IllegalArgumentException("Start and End Times MUST be specified in format HHMM");}
	
	  if(! Pattern.matches("^[0-9]*$", d)){throw new IllegalArgumentException("Date MUST be expressed in numerical form: " + d);}
	  if(! Pattern.matches("^[0-9]*$", t)){throw new IllegalArgumentException("Time MUST be expressed in numerical form: " + t);}

      if (d.length() != 8){throw new IllegalArgumentException("Date Format is wrong, it MUST be YYYYMMDD: " + d);}
      if (t.length() != 4){throw new IllegalArgumentException("Time Format is wrong, it MUST be YYYYMMDD: " + t);}
      
      String YEAR=d.substring(0,4);
      String MONTH=d.substring(4,6);
      String DAY=d.substring(6,8);
      
      String HOUR=t.substring(0,2);
      String MINUTE=t.substring(2,4);
      
      if (Integer.parseInt(MONTH) < 1 || Integer.parseInt(MONTH) > 12 ){throw new IllegalArgumentException("Date MM is wrong, it MUST be between 01 and 12: " + MONTH);}
      if (Integer.parseInt(DAY) < 1 || Integer.parseInt(DAY) > 31 ){throw new IllegalArgumentException("Date DD is wrong, it MUST be between 01 and 31: " + DAY);}

      if (Integer.parseInt(HOUR) < 0 || Integer.parseInt(HOUR) > 23 ){throw new IllegalArgumentException("Hour HH is wrong, it MUST be between 00 and 23: " + HOUR);}
      if (Integer.parseInt(MINUTE) < 0 || Integer.parseInt(MINUTE) > 59 ){throw new IllegalArgumentException("Minute MM is wrong, it MUST be between 00 and 59: " + MINUTE);}
	}
	
      return true;
	
}


	


public static void sayMore(String s){
	
	if(DEBUGMODE){
	System.out.println("DEBUG => "+s);
	}
}
}