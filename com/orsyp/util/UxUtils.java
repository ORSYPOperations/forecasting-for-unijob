package com.orsyp.util;

public class UxUtils {

	public static String FormatInt(int val, int numberOfChar, String toFill)
	{
		String ret=val+"";
		while (ret.length()<numberOfChar)
			ret=toFill+ret;
		return ret;
	}	
}
