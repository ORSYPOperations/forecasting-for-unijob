package com.orsyp.util;

public class UxDateUtil {

	public static String uxDate(String formatIn, String valIn, String formatOut)
	{
		try{
			// Filters will be updated soon...
			//almost..
			String MM="";
			String DD="";
			String YYYY="";
				
			if (formatIn.equals("YYYYMMDD"))
			{
				YYYY=valIn.substring(0,4);
				MM=valIn.substring(4,6);
				DD=valIn.substring(6);
			}
			else if (formatIn.equals("MM/DD/YY"))
			{
				if (valIn.indexOf('/')==1)
					valIn="0"+valIn;

				// Valid until 2099 LOL
				YYYY="20"+valIn.substring(valIn.lastIndexOf('/')+1);
				MM=valIn.substring(0,valIn.indexOf('/'));
				DD=valIn.substring(valIn.indexOf('/')+1,valIn.lastIndexOf('/'));
			} 
			else if (formatIn.equals("DD/MM/YYYY"))
			{
				// Valid until 2099 LOL
				YYYY=valIn.substring(valIn.lastIndexOf('/')+1);
				DD=valIn.substring(0,valIn.indexOf('/'));
				MM=valIn.substring(valIn.indexOf('/')+1,valIn.lastIndexOf('/'));
			} 
			
			return formatOut.replaceAll("MM", MM).replaceAll("DD", DD).replaceAll("YYYY", YYYY);
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
}
