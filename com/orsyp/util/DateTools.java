/*
 * DateTools.java
 *
 * Title: Dollar Universe utilities
 *
 * Description:
 *
 * Copyright: Copyright (c) 2006
 *
 * Company: Orsyp Logiciels Inc.
 */

package com.orsyp.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.TimeZone;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Utility class for dates manipulation.
 * <code>Date</code> objects are computed in <b>GMT</b> time zone.
 *
 * @author jjt
 * @author cml
 * @version $Revision: 1.12 $
 *
 * @see GmtSimpleDateFormat
 */
public class DateTools {

    /** The GMT time zone. */
    static final public TimeZone GMT = TimeZone.getTimeZone("GMT+8");

    /** multithread implementation (no lock) */
    static private ThreadLocal<DateFormat[]> tlsDateFormats = new ThreadLocal<DateFormat[]>() {
        protected DateFormat[] initialValue() {
            return new DateFormat[] {
                    new GmtSimpleDateFormat("yyyyMMdd"),
                    new GmtSimpleDateFormat("HHmmss"),
                    new GmtSimpleDateFormat("yyyyMMddHHmmss"),
                    new SimpleDateFormat("yyyyMMddHHmmss"),
                    new SimpleDateFormat("yyyyMMddHHmm"),
                    new GmtSimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            };
        }
    };

    static private class TLSDateFormat {
        final private int pos;
        private TLSDateFormat(int pos) { this.pos=pos; }
        public DateFormat get() { return tlsDateFormats.get()[pos]; }
    }

    /** DateFormat to be used to format a "yyyyMMdd" date. */
    static final private TLSDateFormat YYYYMMDD = new TLSDateFormat(0);

    /** DateFormat to be used to format a "HHmmss" hour. */
    static final private TLSDateFormat HHMMSS = new TLSDateFormat(1);

    /** DateFormat with GMT timezone to be used to format a "yyyyMMddHHmmss" date-hour. */
    static final private TLSDateFormat YYYYMMDDHHMMSS_GMT = new TLSDateFormat(2);

    /** DateFormat with default timezone to be used to format a "yyyyMMddHHmmss" date-hour. */
    static final private TLSDateFormat YYYYMMDDHHMMSS = new TLSDateFormat(3);

    /** DateFormat with default timezone to be used to format a "yyyyMMddHHmm" date-hour. */
    static final private TLSDateFormat YYYYMMDDHHMM = new TLSDateFormat(4);
    
    /** DateFormat with GMT timezone to be used to format a "yyyy-MM-dd HH:mm:ss" date-hour. */
    static final private TLSDateFormat DATABASEFORMAT = new TLSDateFormat(5);


    /** Undefined value for a date in the format "YYYYMMDD" (8 spaces); */
    public static final String UNDEFINED_YYYYMMDD 		= "        ";
    /** Undefined value for a time in the format "HHMMSS" (6 spaces); */
    public static final String UNDEFINED_HHMMSS			= "      ";
    /** Undefined value for a time in the format "HHMM" (4 spaces); */    
    public static final String UNDEFINED_HHMM			= "    ";

    //--- Constructors -------------------------------------------------------

    /** Suppresses default constructor to prevent instanciation. */
    private DateTools() {
        super();
    }

    //--- Public methods -----------------------------------------------------

    /**
     * Converts a string representation of a date or a date-and-time into a
     * <code>Date</code> object.
     *
     * <p>
     * The date must be in the format <tt>yyyyMMdd</tt>.
     * The date-and-time must be in the format <tt>yyyyMMddHHmmss</tt>.
     *
     * @param yyyymmdd            the string representation of the date or
     *                            date-and-time.
     *
     * @return the corresponding <code>Date</code> object.
     */
    static public Date toDate(String yyyymmdd) {
        Calendar cal=toCalendar(yyyymmdd);
        cal.setTimeZone(TimeZone.getTimeZone("GMT+8"));
        return cal.getTime();
      //  TimeZone tz = TimeZone.getAvailableIDs();
      //  cal.setTimeZone(value);
     //   try {
      //      return yyyymmdd==null || (cal=toCalendar(yyyymmdd))==null ? null : cal.getTime();
      //  } catch( RuntimeException iae ) {
     //       return null;
     //   }
    }

    /**
     * Converts a string representation of a date and time into a
     * <code>Date</code> object.
     *
     * <p>
     * The date must be in the format <tt>yyyyMMdd</tt>.
     * The time must be in the format <tt>HHmmss</tt>.
     *
     * <p>
     * <pre>
     *    toDate(yyyymmdd, hhmmss);
     * </pre>
     * is equivalent (except for performance?) to:
     * <pre>
     *    toDate(yyyymmdd + hhmmss);
     * </pre>
     *
     * @param yyyymmdd            the string representation of the date.
     * @param hhmmss              the string representation of the time.
     *
     * @return the corresponding <code>Date</code> object.
     */
    static public Date toDate(String yyyymmdd, String hhmmss) {
        StringBuffer buff = new StringBuffer(14).append(yyyymmdd);
        if( hhmmss!=null) buff.append(hhmmss);
        return toDate(buff.toString());
    }

    /**
     * Applies the specified time zone to a date or a date-and-time and
     * returns the corresponding <code>Date</code> object.
     *
     * <p> The specified <code>date</code> parameter must formatted with the
     * "yyyyMMdd" pattern for a date, or with the "yyyyMMddHHmmss" pattern for
     * a date-and-time. </p>
     *
     * <p> The <code>tz</code> parameter must be formatted like the
     * management unit time zone: +hhhmm / -hhhmm. </p>
     *
     * @param datehour            the date or date-and-time.
     * @param tz                  the time zone.
     *
     * @return  the corresponding <code>Date</code> object, in GMT.
     */
    static public Date addTimezone(String datehour, String tz) {
        return addTimezone(toCalendar(datehour), tz);
    }

    /**
     * Applies the specified time zone to the specified date and returns the
     * corresponding <code>Date</code> object.
     *
     * <p> The <code>tz</code> parameter must be formatted like the
     * management unit time zone: +hhhmm / -hhhmm. </p>
     *
     * @param date                the date.
     * @param tz                  the time zone.
     *
     * @return  the corresponding <code>Date</code> object, in GMT.
     */
    static public Date addTimezone(Date date, String tz) {
        return addTimezone(toCalendar(date), tz);
    }

    /**
     * Unapplies a time zone from a date.
     * The <code>tz</code> is formatted like the management unit time zone
     * (+hhhmm / -hhhmm).
     *
     * @param date                the date.
     * @param tz                  the time zone.
     *
     * @return  the computed date-and-time, in "yyyyMMddHHmmss" format
     */
    static public String removeTimezone(Date date, String tz) {

        if (tz == null || tz.charAt(0) == ' ') {
            return getYYYYMMDDHHMMSS(date);
        }

        char operator = tz.charAt(0);
        if (operator != '-' && operator != '+') {
            throw new IllegalArgumentException();
        }
        int hour = Integer.parseInt(tz.substring(1, 4));
        int minute = Integer.parseInt(tz.substring(4, 6));
        if (operator == '+') {
            hour = hour * -1;
            minute = minute * -1;
        }

        Calendar calendar = getCalendarInstance();
        calendar.setTime(date);
        calendar.add(Calendar.HOUR_OF_DAY, hour);
        calendar.add(Calendar.MINUTE, minute);
        return getYYYYMMDDHHMMSS(calendar.getTime());
    }

    /** Applies a time zone to a date-and-time and returns the corresponding
     * string representation of the computed date-and-time.
     * The <code>tz</code> is formatted like the management unit time zone
     * (+hhhmm / -hhhmm).
     *
     * @param datehour            the date and time.
     * @param tz                  the time zone.
     *
     * @return  the computed date-and-time, in yyyyMMddHHmmss format.
     */
    static public String addTimezoneStr(String datehour, String tz) {
        return datehour.compareTo("99999999999999")==0 ? datehour :
            getYYYYMMDDHHMMSS(addTimezone(datehour, tz));
    }

    /** Returns the yyyyMMdd-formatted string representation of a given date.
     *
     * @param date                the date to convert (in GMT).
     *
     * @return  the yyyyMMdd-formatted string.
     */
    static public String getYYYYMMDD(Date date) {
        return date==null ? null : YYYYMMDD.get().format(date);
    }

    /** Returns the HHmmss-formatted string representation of a given date.
     *
     * @param date                the date to convert (in GMT).
     *
     * @return  the HHmmss-formatted string.
     */
    static public String getHHMMSS(Date date) {
        return date==null ? null : HHMMSS.get().format(date);
    }

    /** Returns the yyyyMMddHHmmss-formatted string representation of a
     * given date.
     *
     * @param date                the date to convert (in GMT).
     *
     * @return  the yyyyMMddHHmmss-formatted string.
     */
    static public String getYYYYMMDDHHMMSS(Date date) {
        return date==null ? null : YYYYMMDDHHMMSS_GMT.get().format(date);
    }

    /** Returns the yyyyMMddHHmm-formatted string representation of a
     * given date.
     *
     * @param date                the date to convert (in GMT).
     *
     * @return  the yyyyMMddHHmm-formatted string.
     */
    static public String getYYYYMMDDHHMM(Date date) {
        return date==null ? null : YYYYMMDDHHMM.get().format(date);
    }
    
    /** format this date with this pattern "YYYY-MM-DD HH:MI:SS" */
    static public String databaseFormat(Date date) {
        return date==null ? null : DATABASEFORMAT.get().format(date);
    }
    
    /** convert a date stored in a database ("YYYY-MM-DD HH:MI:SS") as a java.util.Date structure or return null if it cannot*/
    static public Date databaseDate(String str) {
        try {
            return str==null ? null : DATABASEFORMAT.get().parse(str.trim());
        }catch(ParseException pe) {
            return null;
        }
    }


    /**
     * Returns a GMT calendar instance.
     *
     * @return a GMT calendar instance.
     */
    static public Calendar getCalendarInstance() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(GMT);
        calendar.setLenient(false);
        return calendar;
    }

    /**
     * Parse the provided date and strip the GMT information, regardless of its
     * value. 
     * 
     * @param date A new date that has the same value as the initial parameter
     *             but the default timezone.
     * @return The new date with default timezone, having the same value as 
     *         the initial parameter. If an exception occurs, the date parameter
     *         is returned. In case the paramete is <code>null</code>, 
     *         then this method returns <code>null</code> also.
     */
    static public Date removeGMTFromDate(Date date) {
        if (date == null) {
            return date;
        } else try {
            String resultAsString = YYYYMMDDHHMMSS_GMT.get().format(date);
            return YYYYMMDDHHMMSS.get().parse(resultAsString);
        } catch (ParseException ignore) {
            return null;
        }
    }
    //--- Protected methods --------------------------------------------------

    final static private Pattern datePattern = Pattern.compile(
            "^\\D*(\\d{4}+)" // year
            +"\\D*(\\d{2}+)"  // month
            +"\\D*(\\d{2}+)"  // day
            +"\\D*(\\d{2}+)?+"  // hour
            +"\\D*(\\d{2}+)?+"  // min
            +"\\D*(\\d{2}+)?+"  // seconds
            +"\\D*(\\d{3}+)?+"  // milliseconds
    );


    final static private Matcher dateMatcher(String date) {
        synchronized(datePattern){
            return datePattern.matcher(date);
        }
    }

    /** Extract date parts from a string whose format is "YYYYMMDDHHMISSMSS"
     * - where YYYY=year must be compound of 4 digits (mandatory).
     * - where MM=month must be compound of 2 digits (mandatory).
     * - where DD=day must be compound of 2 digits (mandatory).
     * - where HH=hour must be compound of 2 digits (optional).
     * - where MI=minutes must be compound of 2 digits (optional).
     * - where SS=seconds must be compound of 2 digits (optional).
     * - where MSS=milliseconds must be compound of 3 digits (optional).
     * 
     * Some separators may occurs between parts of the dates.
     * 
     * 
     * @param   date    date
     * @return  null    if the date is invalid 
     *          [yyyy,mm,dd,hh,mi,ss,mss] else
     */
    final static public String[] split(String date) {
        Matcher m;
        if( date==null || date.matches("^0*$") || !(m=dateMatcher(date)).matches() ) {
            return null;
        } else { 
            String[] tmp = new String[m.groupCount()];
            int len = 0;
            for(len=0;len<tmp.length ;len++) {
                tmp[len]=m.group(len+1);
                if(tmp[len]==null) break;
            }
            String[] ret = new String[len];
            System.arraycopy(tmp, 0, ret, 0, len);
            return ret;
        }
    }

    /** compare date without casting them */
    static public final int compareDates(String d1,String d2) {
        String[] dates1 = DateTools.split(d1);
        String[] dates2 = DateTools.split(d2);
        if( dates1==null ) return -1;
        if( dates2==null ) return +1;
        int len = Math.min(dates1.length,dates2.length);
        for(int i=0;i<len;i++) {
            int n1 = dates1[i]==null ? 0 : Integer.parseInt(dates1[i]);
            int n2 = dates2[i]==null ? 0 : Integer.parseInt(dates2[i]);
            if( n1<n2 ) return -1;
            if( n1>n2 ) return +1;
        }
        return dates1.length-dates2.length;
    }

    static private final int[][] calendarPatterns = new int[][] {
        new int[] { Calendar.YEAR, 0 },
        new int[] { Calendar.MONTH, -1 },
        new int[] { Calendar.DAY_OF_MONTH, 0},
        new int[] { Calendar.HOUR_OF_DAY, 0},
        new int[] { Calendar.MINUTE, 0},
        new int[] { Calendar.SECOND, 0},
        new int[] { Calendar.MILLISECOND, 0}
    };

    /**
     * Returns a <code>Calendar</code> initialized with the given date or
     * date-and-time.
     * The time zone of the returned calendar is GMT.
     *
     * @param datehour            the date (in yyyyMMdd format), or the
     *                            date-and-time (in yyyyMMddHHmmss format).
     *
     * @return  the corresponding calendar.
     */
    final static public Calendar toCalendar(String datehour) {
        String[] parts = split(datehour);
        if( parts==null ) return null;
        Calendar calendar = getCalendarInstance();
        int   max = Math.min(parts.length, calendarPatterns.length);
        for(int i=0;i<max;i++) {
            calendar.set(calendarPatterns[i][0], 
                    parts[i]==null ? 0 : 
                        Integer.parseInt(parts[i])+calendarPatterns[i][1]
            );		    
        }
        for(int i=max;i<calendarPatterns.length;i++) {
            calendar.set(calendarPatterns[i][0],0);          
        }
        return calendar;
    }

    /**
     * Consructs and returns a <code>Calendar</code> initialized with the
     * specified <code>Date</code>.
     * The time zone of the returned calendar is GMT.
     *
     * @param date                a date.
     *
     * @return  the corresponding <code>Calendar</code>.
     */
    final static public Calendar toCalendar(Date date) {
        Calendar calendar = getCalendarInstance();
        calendar.setTime(date);
        return calendar;
    }

    /**
     * Applies the specified time zone to the specified calendar and returns
     * the corresponding <code>Date</code> object.
     *
     * <p> The <code>tz</code> parameter must be formatted like the
     * management unit time zone: +hhhmm / -hhhmm. </p>
     *
     * @param calendar            the calendar to modify.
     * @param tz                  the time zone.
     *
     * @return  the corresponding <code>Date</code> object, in GMT.
     */
    static protected Date addTimezone(Calendar calendar, String tz) {

        if (tz == null || tz.charAt(0) == ' ') {
            return calendar.getTime();
        }

        char operator = tz.charAt(0);
        if (operator != '-' && operator != '+') {
            throw new IllegalArgumentException();
        }
        int hour = Integer.parseInt(tz.substring(1, 4));
        int minute = Integer.parseInt(tz.substring(4, 6));
        if (operator == '-') {
            hour = hour * -1;
            minute = minute * -1;
        }

        calendar.add(Calendar.HOUR_OF_DAY, hour);
        calendar.add(Calendar.MINUTE, minute);
        return calendar.getTime();
    }


    final static public String maxDates(String... dates) {
        String maxDate = null;
        for( String d : dates ) {
            if( DateTools.compareDates(maxDate, d)<0 ) maxDate = d;
        }
        return maxDate;     
    }

    final static public Date maxDates(Date... dates) {
        Date maxDate = null;
        for( Date curDate : dates ) if( curDate!=null ) {
            maxDate =   maxDate==null ? curDate : 
                curDate==null ? maxDate : 
                    maxDate.compareTo(curDate)<0 ? curDate : maxDate; 
        }
        return maxDate;
    }

    final static public String minDates(String... dates) {
        String minDate = null;
        for( String d : dates ) {
            if( DateTools.compareDates(minDate, d)>0 ) minDate = d;
        }
        return minDate;     
    }

    final static public Date minDates(Date... dates) {
        Date minDate = null;
        for( Date curDate : dates ) if( curDate!=null ) {
            minDate =   minDate==null ? curDate : 
                curDate==null ? minDate : 
                    minDate.compareTo(curDate)>0 ? curDate : minDate; 
        }
        return minDate;
    }

    final private static String[] formatPatterns = new String[] {
        "YYYY","MM","DD","HH","MI","SS"
    };

    final static public String format(String date,String outputFormat) {
        String []   parts   = split(date);
        if( parts==null ) return null;
        int         len     = Math.min(parts.length, formatPatterns.length);
        for(int i=0;i<len;i++) {
            date=date.replace(formatPatterns[i], 
                    parts[i]!=null ? parts[i]  :
                        formatPatterns[i].replaceAll(".", "0")
            );
        }
        for(int i=len;i<formatPatterns.length;i++) {
            date=date.replace(formatPatterns[i], formatPatterns[i].replaceAll(".", "0"));
        }
        return date;
    }

    //--- Test method --------------------------------------------------------
    /*
        public static void main(String[] args) {

            String[] test = {
                    "20060201", "19731014", "19731410",
                    "12345678", "00000000", "        ",
                    "20381231", "20060231", "  "
            };

            for (int i = 0; i < test.length; i++) {
                try {
                    System.out.print(test[i] + " --> ");
                    System.out.println(toDate(test[i]));
                } catch (Throwable th) {
                    System.out.println("error: " + th.getClass() + ": " +
                                       th.getMessage());
                }
            }
        }
     */


}
