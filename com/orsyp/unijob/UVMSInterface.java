package com.orsyp.unijob;

import com.orsyp.Area;
import com.orsyp.Environment;
import com.orsyp.Identity;
import com.orsyp.SyntaxException;
import com.orsyp.UniverseException;
import com.orsyp.api.Client;
import com.orsyp.api.Context;
import com.orsyp.api.FunctionalPeriod;
import com.orsyp.api.Product;
import com.orsyp.api.central.UniCentral;
import com.orsyp.api.dqm.DqmQueue;
import com.orsyp.api.node.Node;
import com.orsyp.api.resource.Resource;
import com.orsyp.api.session.Session;
import com.orsyp.api.task.Task;
import com.orsyp.api.uproc.Memorization;
import com.orsyp.api.uproc.Uproc;
import com.orsyp.api.uproc.UprocId;
import com.orsyp.api.uproc.Memorization.Type;
import com.orsyp.api.uproc.cmd.CmdData;
import com.orsyp.std.ClientConnectionManager;
import com.orsyp.std.ConnectionFactory;
import com.orsyp.std.NodeConnectionFactory;
import com.orsyp.std.ResourceStdImpl;
import com.orsyp.std.SessionStdImpl;
import com.orsyp.std.StdImplFactory;
import com.orsyp.std.TaskStdImpl;
import com.orsyp.std.UprocStdImpl;
import com.orsyp.std.central.UniCentralStdImpl;
import com.orsyp.std.dqm.DqmQueueStdImpl;
import com.orsyp.std.security.StdSecurityManager;

public class UVMSInterface {
	// central connection
	protected String UVMShost;
	protected int UVMSport;
	protected String UVMSuser ;
	protected String UVMSpwd ;
	// UniJob environment
	protected String DUCompany;
	protected String DUNode;
	// client ID
	protected String clientGroup = "*";
	protected String clientHost = "*";
	protected String clientSystem = "W32";
	protected Area defArea=Area.Exploitation;
	int iter=1000000;
	int iter_delay=5000;
	
	// Contexte
	Context context;

	private StdImplFactory implFactory;
	private StdSecurityManager securityManager;


	public	UVMSInterface(String UVMShost, int UVMSport, String UVMSuser, String UVMSpwd,
			String DUNode, String DUCompany, Area area) {

		this.UVMShost = UVMShost;
		this.UVMSport = UVMSport;
		this.UVMSuser = UVMSuser;
		this.UVMSpwd = UVMSpwd;
		this.DUNode = DUNode;
		this.DUCompany = DUCompany;
		this.defArea=area;
	}

	public UniCentral ConnectEnvironment() {
		
		UniCentral central = new UniCentral(this.UVMShost, this.UVMSport);
		central.setImplementation(new UniCentralStdImpl(central));

		// login
		try {
			
			System.out.println("  --> Connection to UVMS ["+UVMShost+":"+UVMSport+"] user:["+UVMSuser+"]");
			central.login(this.UVMSuser, this.UVMSpwd);
			central.authenticate();
			System.out.println("  +++ authentication successful\n");
		} catch (UniverseException e) {
			System.out.println("--- login failed. please check Login, Password, UVMS Port and UVMS Hostname.\n\n");
			//e.printStackTrace();
		}

		// get the network node list
		Environment environment = null;
		try {
			environment = new Environment(this.DUCompany, this.DUNode, defArea);

			Client client = new Client(new Identity(this.UVMSuser, clientGroup, clientHost, clientSystem));

			this.context = new Context(environment, client, central);
			ConnectionFactory factory = NodeConnectionFactory.getInstance(central);
			ClientConnectionManager.setDefaultFactory(factory);

			securityManager = new StdSecurityManager();
			implFactory = new StdImplFactory(factory, false);
			this.context.setProduct (Product.UNICENTRAL);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return central;

	}

	public void createUproc(Uproc upr) throws UniverseException {
		try{
			upr.setContext(this.getContext());
			UprocStdImpl uprocStdImpl = new UprocStdImpl();
			uprocStdImpl.init(upr);
			upr.setImpl(uprocStdImpl);
			upr.create();
		} 
		catch (com.orsyp.api.ObjectAlreadyExistException oaee) {
			//TODO Do not output anything now.
			//oaee.printStackTrace();
		}		
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}

	//public String getDUCompany() {
	//	return DUCompany;
	//}

	//public void setDUCompany(String company) {
	//	DUCompany = company;
	//}

	//public String getDUNode() {
	//	return DUNode;
	//}

	//public void setDUNode(String node) {
	//	DUNode = node;
	//}

	public StdImplFactory getImplFactory() {
		return implFactory;
	}

	public void setImplFactory(StdImplFactory implFactory) {
		this.implFactory = implFactory;
	}

	public int getIter() {
		return iter;
	}

	public void setIter(int iter) {
		this.iter = iter;
	}

	public int getIter_delay() {
		return iter_delay;
	}

	public void setIter_delay(int iter_delay) {
		this.iter_delay = iter_delay;
	}

	public StdSecurityManager getSecurityManager() {
		return securityManager;
	}

	public void setSecurityManager(StdSecurityManager securityManager) {
		this.securityManager = securityManager;
	}

	public String getUVMShost() {
		return UVMShost;
	}

	public void setUVMShost(String shost) {
		UVMShost = shost;
	}

	public int getUVMSport() {
		return UVMSport;
	}

	public void setUVMSport(int sport) {
		UVMSport = sport;
	}

	public String getUVMSpwd() {
		return UVMSpwd;
	}

	public void setUVMSpwd(String spwd) {
		UVMSpwd = spwd;
	}

	public String getUVMSuser() {
		return UVMSuser;
	}

	public void setUVMSuser(String suser) {
		UVMSuser = suser;
	}

	public String getClientGroup() {
		return clientGroup;
	}

	public void setClientGroup(String clientGroup) {
		this.clientGroup = clientGroup;
	}

	public String getClientHost() {
		return clientHost;
	}

	public void setClientHost(String clientHost) {
		this.clientHost = clientHost;
	}

	public String getClientSystem() {
		return clientSystem;
	}

	public void setClientSystem(String clientSystem) {
		this.clientSystem = clientSystem;
	}

	public Area getDefArea() {
		return defArea;
	}

	public void setDefArea(Area defArea) {
		this.defArea = defArea;
	}

	
}