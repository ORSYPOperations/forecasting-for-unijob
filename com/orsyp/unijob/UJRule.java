package com.orsyp.unijob;

import com.orsyp.api.rule.DayShift;
import com.orsyp.api.rule.KmeleonPattern.KDayType;
import com.orsyp.api.rule.PositionsInPeriod;
import com.orsyp.api.rule.PositionsInPeriod.SubPeriodType;
import com.orsyp.api.rule.Rule.PeriodTypeEnum;

public class UJRule {

	public int periodNum;
	public PeriodTypeEnum periodType;
	
	public PositionsInPeriod posInPeriod;
	public int subPeriodNum;
	public SubPeriodType subPeriodType;
	
	public boolean[] weekMask = new boolean[7];
	
	public boolean[] monthMask = new boolean[12];
	
	public int WDayAuth; //0, 1 or 2
	public DayShift WShift;
	public KDayType WOffDayType;
	
	public int CDayAuth; //0, 1 or 2
	public DayShift CShift;
	public KDayType COffDayType;
	
	public int HDayAuth; //0, 1 or 2
	public DayShift HShift;
	public KDayType HOffDayType;

public UJRule(){
	// empty constructor ??
}

public String getRuleDescription(){
	
	String s="[";
	s=s+this.periodNum+this.periodType.name()+"]";
	
	s=s+"[";
	if(posInPeriod.isForward()){s=s+"+";}
	else{s=s+"-";}
	// need to replace , by & because of the format of the output file.. (csv)
	s=s+posInPeriod.getPositionsPattern().replaceAll(", ","&")+posInPeriod.getType().toString();
	s=s+"]";
	
	s=s+"["+convertToString(weekMask)+"]";
	
	s=s+"["+convertToString(monthMask)+"]";
	
	s=s+"["+WDayAuth;
	if(!(WShift==null)){s=s+WShift.getShiftNumber()+WOffDayType.toString()+"]";}
	else{s=s+"+0"+"NONE"+"]";}
	s=s+"["+CDayAuth;
	if(!(CShift==null)){s=s+CShift.getShiftNumber()+COffDayType.toString()+"]";}
	else{s=s+"+0"+"NONE"+"]";}
	s=s+"["+HDayAuth;
	if(!(HShift==null)){s=s+HShift.getShiftNumber()+HOffDayType.toString()+"]";}
	else{s=s+"+0"+"NONE"+"]";}
	
	return s;
	
//	+posInPeriod.getPositionsPattern()+
	
}

private String convertToString(boolean[] b) {
	String S="";
	for(int i=0;i<b.length;i++){
		if(b[i]){S=S+"1";}
		else{S=S+"0";}
	}
	return S;
}



}
