package com.orsyp.unijob;

import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;
import java.util.TimeZone;
import java.util.Vector;

import com.orsyp.Go;
import com.orsyp.UniverseException;
import com.orsyp.api.job.Job;
import com.orsyp.api.rule.KDayAuthorization;
import com.orsyp.api.rule.KmeleonPattern;
import com.orsyp.api.rule.KmeleonPattern.KDayType;
import com.orsyp.api.rule.MonthAuthorization;
import com.orsyp.api.rule.PositionsInPeriod;
import com.orsyp.api.rule.WeekAuthorization;
import com.orsyp.api.rule.YearAuthorization;
import com.orsyp.api.rule.PositionsInPeriod.SubPeriodType;
import com.orsyp.api.rule.Rule.PeriodTypeEnum;
import com.orsyp.api.task.DetailedSimulation;
import com.orsyp.api.task.ExclusionDate;
import com.orsyp.api.task.ExclusionInterval;
import com.orsyp.api.task.ExplicitLaunchDate;
import com.orsyp.api.task.LaunchHourPattern;
import com.orsyp.api.task.SimpleLaunch;
import com.orsyp.api.task.Simulation;
import com.orsyp.api.task.TaskImplicitData;
import com.orsyp.api.task.TaskPlanifiedData;
import com.orsyp.util.DateTools;
import com.orsyp.util.Pattern;
import com.orsyp.util.PropertyLoader;
import com.orsyp.util.Run;

public class UJSchedPattern {

	public String SubTarget;
	public String SubUser;

	public String UJJob;
	public Job myJob;
	public TaskPlanifiedData TASKPLANDATA;
	public TaskImplicitData[] TASKIMPDATA;
	public LaunchHourPattern[] LNCHRPATTERN;
	public ExclusionDate[] EXCLDATES;
	public ExplicitLaunchDate[] EXPLDATES;
	public ExclusionInterval[] EXCLINTERVALS;
	//public Vector<KmeleonPattern> KMELEONPATTERNS = new Vector<KmeleonPattern>();
	public Vector<FullUjRule> UnijobRules = new Vector<FullUjRule>();
	
	
	public int numLaunchTimes;
	public int numDayOrExcPattern;
	public int numExplicitDate;
	public int numExclInterval;
	public int numExclDate;
	
	public boolean hasPatterns=false;
	public boolean hasExceptions=false;
	public boolean hasLaunchTimes=false;
	public boolean hasDayOrExclPatterns=false;
	public boolean hasExplicitDates=false;
	public boolean hasExclIntervals=false;
	public boolean hasExclDates=false;
	
	public Vector<String[]> allDayPatterns = new Vector<String[]>();
	public Vector<String[]> allLaunchTimes = new Vector<String[]>();
	public Vector<String> GroupedPatterns = new Vector<String>();
	public String ENDDATE;
	public String STARTDATE;
	public String ENDTIME;
	public String STARTTIME;
	
	public UJSchedPattern(Job myJob, String sDate, String eDate, String sTime, String eTime) throws UniverseException{
		this.myJob = myJob;
		this.STARTDATE=sDate;
		this.ENDDATE=eDate;
		this.STARTTIME=sTime;
		this.ENDTIME=eTime;
		this.TASKPLANDATA=(TaskPlanifiedData) myJob.getPlanification();
		this.TASKIMPDATA = this.TASKPLANDATA.getImplicitData();
		this.LNCHRPATTERN = this.TASKPLANDATA.getLaunchHourPatterns();
		this.EXCLDATES = this.TASKPLANDATA.getExclusionDates();
		this.EXPLDATES = this.TASKPLANDATA.getExplicitDates();
		this.EXCLINTERVALS = this.TASKPLANDATA.getExclusionIntervals();
		this.TASKPLANDATA.getMultipleLaunch();

		
		for (int l=0;l<this.TASKIMPDATA.length;l++){
			FullUjRule Obj=new FullUjRule(this.TASKIMPDATA[l]);
			UnijobRules.add(Obj);
			
			//KMELEONPATTERNS.add(this.TASKIMPDATA[l].getKmeleonPattern());

		}
		this.numLaunchTimes=this.LNCHRPATTERN.length;
		this.numDayOrExcPattern=this.UnijobRules.size();
		this.numExplicitDate=this.EXPLDATES.length;
		this.numExclInterval=this.EXCLINTERVALS.length;
		this.numExclDate=this.EXCLDATES.length;
		
		if((numExplicitDate + numExclDate + numExclInterval)>0){
			hasExceptions=true;
			if(numExplicitDate>0){hasExplicitDates=true;}
			if(numExclDate>0){hasExclDates=true;}
			if(numExclInterval>0){hasExclIntervals=true;}
		}
		
		if((numLaunchTimes+numDayOrExcPattern)>0){
			hasPatterns=true;
			if(numLaunchTimes>0){hasLaunchTimes=true;}
			if(numDayOrExcPattern>0){hasDayOrExclPatterns=true;}
		}
		
		if (hasPatterns & numDayOrExcPattern > 0) {
			
			for (int p=0;p<numDayOrExcPattern;p++){
				String TmpPattern="";
				String tmpTime = "";
				
				String[] oneDayPattern = new String[7];
				
				//PositionsInPeriod myPos = this.UnijobRules.get(p).getKPattern().getPositionsInPeriod();
				//if(this.UnijobRules.get(p).getPosition().getType()==SubPeriodType.

				String myPos=this.UnijobRules.get(p).getWeekPattern();
				
				String posName="th";
				
				//String PositionPattern = this.UnijobRules.get(p).getWeekPattern();
				
				if (myPos.endsWith("3")){posName="rd";}
				if (myPos.endsWith("1")){posName="st";}
				if (myPos.endsWith("2")){posName="nd";}
				
				// BSA - Dec 16th - Correction - initializing all fields (strings) within table because of a weird issue encountered only on some versions of Java
				
				String St0=Integer.toString(this.UnijobRules.get(p).getPeriodNumber());
				String St1=this.UnijobRules.get(p).getPeriodType().toString();
				String St2=myPos+posName;
				String St3=this.UnijobRules.get(p).getPosition().getType().toString();
				
				if(!St0.isEmpty() && St0!=null){oneDayPattern[0]=St0;}else{oneDayPattern[0]="";} // Every n...
				if(!St1.isEmpty() && St1!=null){oneDayPattern[1]=St1;}else{oneDayPattern[1]="";}   //this.UnijobRules.get(p).getPeriodType().toString(); // .. Week / Month
				if(!St2.isEmpty() && St2!=null){oneDayPattern[2]=St2;}else{oneDayPattern[2]="";}//myPos+posName; // on 3rd, 4th
				if(!St3.isEmpty() && St3!=null){oneDayPattern[3]=St3;}else{oneDayPattern[3]="";}//this.UnijobRules.get(p).getPosition().getType().toString(); // Monday / Tuesday
				
				//System.out.println("DEBUG0:"+this.UnijobRules.get(p).getPeriodType().toString());
				
				
				if(this.UnijobRules.get(p).getPosition().isForward()){oneDayPattern[4] = "From Beginning";}
				if(! this.UnijobRules.get(p).getPosition().isForward()){oneDayPattern[4] = "From End";}
				
				if(oneDayPattern[4].isEmpty()|| oneDayPattern[4]==null){oneDayPattern[4]=" ";}
				 
				tmpTime = this.UnijobRules.get(p).getKPattern().getSimpleLaunch().getLaunchTime();
				
				if (! tmpTime.isEmpty()){
					oneDayPattern[5] = tmpTime.substring(0, 4);
					}
				else{
					oneDayPattern[5] = "";
				}
				oneDayPattern[6]="-";
				if(this.TASKIMPDATA[p].isAuthorized()){oneDayPattern[6]="+";}
				
				allDayPatterns.add(oneDayPattern);
				
				//TmpPattern = TmpPattern+"Every "+this.TASKIMPDATA[p].getPeriodNumber();
				//TmpPattern = TmpPattern+" "+this.TASKIMPDATA[p].getPeriodType();
				//this.TASKIMPDATA[p].getPeriodNumber();
				//TmpPattern = TmpPattern+" On "+ myPos.getPositionsPattern()+posName+ " " + myPos.getType();
				//this.DayPatterns.add(TmpPattern);
			}
			
			if (hasPatterns & numLaunchTimes > 0) {
				
				for (int p=0;p<numLaunchTimes;p++){
					String TmpPattern="";
					String tmpTime = "";
					String[] oneLaunchTime = new String[4];
					if(this.LNCHRPATTERN[p].getFrequency() == 0){
						oneLaunchTime[0] = "SIMPLE";
						oneLaunchTime[1] = this.LNCHRPATTERN[p].getStartTime().substring(0, 4);
						oneLaunchTime[2] = ""; // every 50 minutes
						oneLaunchTime[3] = ""; // until 22:00	
					}
					else{
						oneLaunchTime[0] = "CYCLICAL";
						oneLaunchTime[1] = this.LNCHRPATTERN[p].getStartTime().substring(0, 4); // From 14:00
						oneLaunchTime[2] = Integer.toString((this.LNCHRPATTERN[p].getFrequency())); // every 50 minutes
						oneLaunchTime[3] = this.LNCHRPATTERN[p].getEndTime().substring(0, 4); // until 22:00
					}
					this.allLaunchTimes.add(oneLaunchTime);
						
				//	if(this.LNCHRPATTERN[p].getFrequency() == 0){TmpPattern = TmpPattern+"At: "+this.LNCHRPATTERN[p].getStartTime();}
				//	else{
				//	TmpPattern = TmpPattern+"From: "+this.LNCHRPATTERN[p].getStartTime().substring(0, 4);
				//	TmpPattern = TmpPattern+" Every: "+this.LNCHRPATTERN[p].getFrequency()+"min";
				//	TmpPattern = TmpPattern+" Until: "+this.LNCHRPATTERN[p].getEndTime().substring(0, 4);
					
				//	}				
					//this.LaunchTimes.add(TmpPattern);					
				}
			}
			
			for (int z=0; z< this.allDayPatterns.size();z++){
				String tmpStr="";
				//tmpStr=this.allDayPatterns.get(z);
				//for (int y=0;y< this.LaunchTimes.size();y++){
				//	tmpStr=tmpStr+" & "+this.LaunchTimes.get(y);
				//}
			this.GroupedPatterns.add(tmpStr);
			//System.out.println(tmpStr);
			}
		} else {
			this.GroupedPatterns.add("NO PATTERN - ON DEMAND JOB");
		}
	}
		//int lengthDayWindow = Integer.parseInt(endDate) - Integer.parseInt(beginDate);
		//if(lengthDayWindow<0){System.out.println("ERROR !");}
		//boolean RunFlag = isJobWithinWindow(myJob, DateTools.toDate(beginDate),DateTools.toDate(endDate));
		//Simulation JobSim = myJob.simulate(DateTools.toDate(beginDate), DateTools.toDate(endDate));
		//DetailedSimulation JobSimDetailed = myJob.simulateFull(DateTools.toDate("201201150000"), DateTools.toDate("201201160000"));
		//Date[] myDates = JobSim.getForecastDates();
		//Date[] myExclDates = JobSim.getExcludedDates();
	//	Run[] myRuns = myJob.simulateFull(DateTools.toDate("201201150000"), DateTools.toDate("201201160000")).getRuns(DateTools.toDate("201201150000"));

		//for (int r=0;r<myRuns.length;r++){
		//	System.out.println("Run:"+myRuns[r].getTime());
		//}		
		//System.out.println("DEBUGvdsf:"+hasPatterns+":"+hasExceptions);
		//KmeleonPattern r = this.TASKIMPDATA[0].getKmeleonPattern();
		//Hashtable<KDayType,KDayAuthorization> ht = r.getDayAuthorizations();		
	//}
	
	/**
	 * 
	 * We want to convert a pattern such as: 0020, 0820, 1020, 1220, 1420, 1620, 1820, 2020, 2220
	 * into: from 0820 to 2220 every 2 hours and at 0020
	 * 
	 * 
	 */
	
	
	public String ConvertSimpleToCyclicalPattern(){
		boolean isSimple=true;
		Vector<Integer> allLaunches = new Vector<Integer>();
		// Number of Launch Hour Patterns
		Go.sayMore(""+numLaunchTimes);
		for(int i=0;i<this.allLaunchTimes.size();i++){
			String[] myData = this.allLaunchTimes.get(i);
			Go.sayMore(""+myData[0]);
			if(!myData[0].equals("SIMPLE")){isSimple=false;}
			if(isSimple){
				Go.sayMore(""+Integer.valueOf(myData[1]));
				allLaunches.add(Integer.valueOf(myData[1]));
			}
		}
		int StartTime=allLaunches.get(0);
		Go.sayMore(""+StartTime+":"+allLaunches.size()+":");
		int EndTime=allLaunches.get(allLaunches.size()-1);
		int Interval=allLaunches.get(1)-allLaunches.get(0);
		
		String StartTimeTxt=String.valueOf(StartTime);
		if(String.valueOf(StartTime).length()==1){StartTimeTxt="000"+String.valueOf(StartTime);}
		if(String.valueOf(StartTime).length()==2){StartTimeTxt="00"+String.valueOf(StartTime);}
		if(String.valueOf(StartTime).length()==3){StartTimeTxt="0"+String.valueOf(StartTime);}
			
		String IntervalText="";
		String HrText="";
		String MnText="";
		int Hr=0;
		int Mn=0;
		Go.sayMore(""+Interval);
		if(Interval>=100){
			Hr=Interval/100;
			Mn=Interval-Hr*100;
		}else{
			Hr=0;
			Mn=Interval;
		}

		if(String.valueOf(Hr).length()==1){HrText="0"+String.valueOf(Hr);}else{HrText=String.valueOf(Hr);}
		if(String.valueOf(Mn).length()==1){MnText="0"+String.valueOf(Mn);}else{MnText=String.valueOf(Mn);}

		return StartTimeTxt + " UNTIL: "+EndTime+" EVERY: "+HrText+":"+MnText;
	}
	
	/** 
	 **   !! Watch out for crazy bug on simulateFull method below...
	 **/
	public Vector <Run> GetRunsWithinWindow() throws UniverseException {

		Vector <Date> allDates = new Vector<Date>();
		
		// 1- Start Date and End Dates without timestamp (hour is 0000)
		Date tmpD=DateTools.toDate(STARTDATE);
		Date tmpE=DateTools.toDate(ENDDATE);
		
		Go.sayMore( "Start Short Date is: "+tmpD.toString());
		Go.sayMore( "Stop Short Date is: "+tmpE.toString());
		
		// 2- we add minutes and hours based on what is passed by parameters
		Date tmpDFULL=DateTools.toDate(STARTDATE+STARTTIME);
		Date tmpEFULL=DateTools.toDate(ENDDATE+ENDTIME);
		
		// BSA - Dec 14th 2012 - Bug fix, shifting start time and end time by + and - 1 min 
		Date tmpDFULLA=(Date) tmpDFULL.clone();
		tmpDFULLA.setTime(tmpDFULL.getTime() - 60 * 1000 + 1 * 24 * 60 * 60 * 1000);
		
		Date tmpEFULLA=(Date) tmpEFULL.clone();
		tmpEFULLA.setTime(tmpEFULL.getTime() + 60 * 1000 + 1 * 24 * 60 * 60 * 1000);
		
		
		tmpD.setTime(tmpD.getTime() + 1 * 24 * 60 * 60 * 1000);


		Go.sayMore( "Start Full Date is: "+tmpDFULL.toString());
		Go.sayMore( "Stop Full Date is: "+tmpEFULL.toString());
		Go.sayMore( "Start Full Date is: "+tmpDFULLA.toString());
		Go.sayMore( "Stop Full Date is: "+tmpEFULLA.toString());
		
		// 3- We create a table of dates, we put the begin date first then.. 
		allDates.add(new Date(tmpD.getTime()));
		
		// 4- while begin date and end date not equal..
		while(tmpD.before(tmpE)){
		// 5- We add one day and then we add it to the table
			tmpD.setTime(tmpD.getTime() + 1 * 24 * 60 * 60 * 1000);
			allDates.add(new Date(tmpD.getTime()));
		}
		
		Go.sayMore( "Number of Dates found: "+allDates.size());
		
		// 6- at this point we have a table of dates. Ex between Jan 1st and Jan 3rd, we have a table of 3 dates, Jan 1st, 2nd and 3rd 
		Vector<Run> allValidExec = new Vector<Run>();
		Vector<Run> allRuns = new Vector<Run>();
		
		if(allDates.size()>0){

			for (int dn=0;dn<allDates.size();dn++){
				
				Go.sayMore( "Current Date Being Processed: "+allDates.get(dn));
				//For each Day, we declare two dates, one from beginning of day (St) one for end of day(En)
				Date St = (Date) allDates.get(dn).clone();
				Date En = (Date) allDates.get(dn).clone(); 
				
				Go.sayMore( "Start Date Being Processed: "+St);
				Go.sayMore( "End Date Being Processed: "+En);
				
				/**
				 * The SimulateFull method returns Runs with a one day offset.. if a run is scheduled on a monday, simulateFull will return an equivalent run on... Tuesday
				 *    THEREFORE:
				 * We Offset EVERYTHING by one day because the simulation does not work properly:
				 * Original Start Date: Monday 1st, 0000 => New Etart Date: Tuesday 2nd, 0000
				 * Original End Date: Monday 1st, 0000 => New End Date: Wednesday 3nd, 0000
				 */
				
				St.setTime(St.getTime());//  + 1 * 24 * 60 * 60 * 1000); //+24hr
				En.setTime(En.getTime()   + 1 * 23 * 60 * 60 * 1000 + 59*60*1000);// + 1 * 24 * 60 * 60 * 1000); //+24hr + 23hr59min
				
				
				
				Go.sayMore( "Start Date Being Processed After OPER: "+St);
				Go.sayMore( "End Date Being Processed After OPER: "+En);
				
				Run[] myRuns = myJob.simulateFull(
						St,
						En
						).getRuns(St);

				for(int idx=0;idx<myRuns.length;idx++){
					//Date tmpDate=myRuns[idx].getTime();
					allRuns.add(myRuns[idx]);
				}
				
				Go.sayMore( "Number of Runs found: "+allRuns.size());
				
			}
			
			
			for (int myK=0;myK<allRuns.size();myK++){
				Date tmpDate = allRuns.get(myK).getTime();
				tmpDate.setTime(tmpDate.getTime());// - 1 * 24 * 60 * 60 * 1000);
				Go.sayMore("DEBUG AFTER:"+tmpDFULLA);
				Go.sayMore("DATE BEFORE:"+tmpEFULLA);
				Go.sayMore("ACTUAL DATE:"+tmpDate+" STATUS: "+tmpDate.after(tmpDFULLA)+" : "+tmpDate.before(tmpEFULLA));
				if(tmpDate.after(tmpDFULLA) && tmpDate.before(tmpEFULLA)){
					allValidExec.add(allRuns.get(myK));
				}
			}
				
			}
if(allValidExec.isEmpty()){
	return null;
	}
else{
	return allValidExec;
}

	}

	public void getSubTarget(Job j){
		this.SubTarget = j.getTargetName();
	}

	public void getSubUser(Job j){
		this.SubUser = j.getUserName();
	}
}
