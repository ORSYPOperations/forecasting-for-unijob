package com.orsyp.unijob;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.orsyp.Go;
import com.orsyp.OutcomeReport;
import com.orsyp.UniverseException;
import com.orsyp.api.Context;
import com.orsyp.api.job.Job;
import com.orsyp.api.job.JobId;
import com.orsyp.api.job.JobItem;
import com.orsyp.api.job.JobStatus;
import com.orsyp.api.syntaxerules.UnijobSyntaxRules;
import com.orsyp.specfilters.JobSpecFilter;
import com.orsyp.specfilters.SpecFilter;
import com.orsyp.specfilters.TargetSpecFilter;
import com.orsyp.specfilters.UserSpecFilter;
import com.orsyp.std.JobListStdImpl;
import com.orsyp.std.JobStdImpl;
import com.orsyp.util.Run;

public class UJJobMgt {
	private String nodeName;
	private Context context;
	private Context UJcontext;
	public OutcomeReport rep;
	public String TimeShift;
	public String op;
	public int num;
	public SpecFilter[] allFilters;
	public boolean verbose;
	public com.orsyp.api.job.JobList UJJobList;

	public UJJobMgt(String nodeName, int uJPort, Context context,
			Context UJcontext, SpecFilter[] allFilters, boolean verbose)
			throws UniverseException {
		this.nodeName = nodeName;
		this.context = context;
		this.UJcontext = UJcontext;
		this.allFilters = allFilters;
		this.verbose = verbose;
		this.UJJobList = this.getJobListUJ(this.UJcontext);
	}
	
	private com.orsyp.api.job.JobList getJobListUJ(Context c){
		com.orsyp.api.job.JobList jl = new com.orsyp.api.job.JobList(
				this.UJcontext);
		// Dec 2013 - BSA - Adaptation to UV6 (ex UV4)
		jl.setSyntaxRules(UnijobSyntaxRules.getInstance());
		jl.setImpl(new JobListStdImpl());
		try {
			jl.extract();
		} catch (UniverseException u) {
			//System.out.println("Error!");
		}
		return jl;
	}
	private Job getUJJob(JobItem it) throws UniverseException{
		JobId ID = it.getIdentifier();
		Job Ujob = new Job(UJcontext, ID);
		Ujob.setImpl(new JobStdImpl());
		Ujob.extract();
		return Ujob;
		
	}

	// Parsing the list of all jobs
	public void parseJobs(Boolean DATE_ACTIVE, String sDate, String sTime,
			String eDate, String eTime) throws UniverseException {
		
		int jlcount = 0;
		jlcount = this.UJJobList.getCount();

		System.out
				.println(" **************************************************************************************");
		System.out
				.println(" **                                   Reporting Mode                                 **");
		System.out
				.println(" **************************************************************************************");

		System.out.println("  NUMBER OF JOBS FOUND:" + jlcount);

		// BSA - Improvement - considering object Job to retrieve more info in
		// forecast
		
		// Hashmap is used for forecasting
		HashMap<Job, Vector<Run>> AllValidRuns = new HashMap<Job, Vector<Run>>();	
		
		for (int j = 0; j < this.UJJobList.getCount(); j++) {
			
			Job Ujob = getUJJob(this.UJJobList.get(j));
			
			// Applying filters here...
			// NodeSpecFilter nfilter = (NodeSpecFilter) allFilters[0];
			JobSpecFilter jfilter = (JobSpecFilter) allFilters[1];
			TargetSpecFilter tfilter = (TargetSpecFilter) allFilters[2];
			UserSpecFilter ufilter = (UserSpecFilter) allFilters[3];
			if (!jfilter.matchespattern(Ujob.getName()) || !tfilter.matchespattern(Ujob.getTargetName()) || !ufilter.matchespattern(Ujob.getUserName())) {
				if (verbose) {
					System.out.println("%%% Job " + Ujob.getName() + " skipped.");
				}
			}

			else if (jfilter.matchespattern(Ujob.getName())
					&& tfilter.matchespattern(Ujob.getTargetName())
					&& ufilter.matchespattern(Ujob.getUserName())) {

				String TmpLaunchTimesDisplay = "";
				
				// THE FOLLOWING LINES ARE HERE FOR FORECASTING ONLY
				UJSchedPattern JobPattern = new UJSchedPattern(Ujob, sDate, eDate, sTime, eTime);
				Vector<Run> Runs = new Vector<Run>();
				Runs = JobPattern.GetRunsWithinWindow();
				if (Ujob.getStatus() == JobStatus.ENABLED) {
					AllValidRuns.put(Ujob, Runs);
				}
				//
				
				Vector<String[]> allDayPatterns = JobPattern.allDayPatterns; // Ex of a day pattern: Every[0] Week[1] on 4th[2] Monday[3] Forward[4] at 04:00[5](optional)
				Vector<String[]> allLaunchTimes = JobPattern.allLaunchTimes; // Ex of a launch time: SIMPLE/CYCLICAL[0] at/From 05:00[1] Every 60minutes[2](optional) until 22:00[3](optional)

				for (int i = 0; i < allLaunchTimes.size(); i++) {
					if (allLaunchTimes.get(i)[0].equals("SIMPLE")) {
						TmpLaunchTimesDisplay = TmpLaunchTimesDisplay + allLaunchTimes.get(i)[1] + " & ";
					} else
						TmpLaunchTimesDisplay = TmpLaunchTimesDisplay
								+ "FROM: "
								+ allLaunchTimes.get(i)[1].substring(0,
										allLaunchTimes.get(i)[1].length() - 1)
								+ " EVERY: " + allLaunchTimes.get(i)[2]
								+ " UNTIL: " + allLaunchTimes.get(i)[3];
				}

				// We have a string containing all launch times patterns
				String LaunchTimesDisplay = TmpLaunchTimesDisplay;
				if(allDayPatterns.size()>1){
					Go.sayMore("WARNING! Job "+Ujob.getName()+" contains "+allDayPatterns.size());
				}
				
				
				List<String> AllDayPatternsAsText = new ArrayList<String>();
				for (int i = 0; i < allDayPatterns.size(); i++) {

					// BSA - Feb 2013 - Adding Username and Status in report
					String DayPatternTempDisplay = "";
					DayPatternTempDisplay = " " + Ujob.getName() + " , " + Ujob.getUserName()
							+ " , " + Ujob.getStatus() + " , "
							+ allDayPatterns.get(i)[6] + " EVERY: "
							+ allDayPatterns.get(i)[0]
							+ allDayPatterns.get(i)[1] + " ON: "
							+ allDayPatterns.get(i)[2].replace(",", " &") + " "
							+ allDayPatterns.get(i)[3] + " "
							+ allDayPatterns.get(i)[4] + " AT:";
					AllDayPatternsAsText.add(DayPatternTempDisplay);
				}
				
				// 15 ? why 15 ? completely arbitrary, se below
				// BSA - Mar 2014 - Bug fix. Factorization of complex pattern does not work with patterns such as: 0020, 0820, 1020, 1220, 1420, 1620, 1820, 2020, 2220
				// The factorization could be improved.. but i am not convinced that it is a required feature and it introduces complexity.
				// we will from now on only factorize when there is more than 15 runs a day
				
				if (allLaunchTimes.size() > 15) {
					LaunchTimesDisplay = JobPattern.ConvertSimpleToCyclicalPattern();
					Go.sayMore(LaunchTimesDisplay);
				}
				String Times = LaunchTimesDisplay.replace(",", "");
				if (Times.length() > 0) {
					Times = Times.substring(0, Times.length() - 1);
				}

				Pattern p = Pattern.compile("\\& $");
				Matcher m = p.matcher(LaunchTimesDisplay);

				for(int i=0;i<AllDayPatternsAsText.size();i++){
				System.out.println(" " + AllDayPatternsAsText.get(i) + " " + m.replaceAll("") + " , " + Ujob.getCommand());
				}
			}
		}
		if (DATE_ACTIVE) {

			System.out.println("\n");
			System.out
					.println(" **************************************************************************************");
			System.out
					.println(" **                                  Forecasting Mode                                **");
			System.out.println(" **          Jobs Forecasted To Run Between "
					+ sDate + "-" + sTime + " and " + eDate + "-" + eTime
					+ "          **");
			System.out
					.println(" **************************************************************************************");



			Iterator myIt = AllValidRuns.entrySet().iterator();

			while (myIt.hasNext()) {
				Map.Entry pair = (Map.Entry) myIt.next();
				Job myJob = (Job) pair.getKey();
				Vector<Run> myRuns = (Vector<Run>) pair.getValue();

				String AllMyRuns = "";
				Iterator myR = myRuns.iterator();
				while (myR.hasNext()) {
					Run run = (Run) myR.next();

					// WATCH OUT.. TIME IS F***ED UP, NEED TO SUBSTRACT 24 HRS
					// AllMyRuns=AllMyRuns+" "+run.getTime();
					Date myDate = (Date) run.getTime();
					Date myRectifiedDate = (Date) myDate.clone();
					myRectifiedDate.setTime(myDate.getTime() - 1 * 24 * 60 * 60
							* 1000);
					AllMyRuns = AllMyRuns + " " + myRectifiedDate;
				}

				System.out.println("  " + myJob.getName() + ", "
						+ myJob.getUserName() + " ," + myJob.getStatus() + ", "
						+ AllMyRuns + ", " + myJob.getCommand());
			}

			System.out.println("\n  End Of Forecast.");
		}
	
	}

}