package com.orsyp.unijob;

import com.orsyp.api.rule.KmeleonPattern;
import com.orsyp.api.rule.MonthAuthorization;
import com.orsyp.api.rule.PositionsInPeriod;
import com.orsyp.api.rule.PositionsInPeriod.SubPeriodType;
import com.orsyp.api.rule.Rule.PeriodTypeEnum;
import com.orsyp.api.rule.Rule;
import com.orsyp.api.rule.WeekAuthorization;
import com.orsyp.api.rule.YearAuthorization;
import com.orsyp.api.task.TaskImplicitData;

public class FullUjRule {

	private TaskImplicitData TIData;
	private KmeleonPattern KPattern;
	private WeekAuthorization WeekAuth;
	private MonthAuthorization MonthAuth;
	private YearAuthorization YearAuth;
	private PositionsInPeriod PosInPeriod;
	private SubPeriodType PosType;
//	private 
	private boolean[] WeekPattern = new boolean[7];
	private boolean[] MonthPattern = new boolean[12];
	
	public FullUjRule(TaskImplicitData t){
		
		this.TIData=t;
		this.WeekAuth=this.TIData.getWeekAuthorization();
		this.MonthAuth=this.TIData.getMonthAuthorization();
		this.YearAuth=this.TIData.getYearAuthorization();
		this.KPattern=this.TIData.getKmeleonPattern();
		
		this.PosInPeriod=this.KPattern.getPositionsInPeriod();
		
		this.PosType=this.PosInPeriod.getType();
		
		for(int i=0;i<this.WeekPattern.length;i++){
			this.WeekPattern[i]=false;
		}
	}
	
	public PositionsInPeriod getPosition(){
		return this.PosInPeriod;
	}
	public int getPeriodNumber(){
		return this.TIData.getPeriodNumber();
	}
	public Rule.PeriodTypeEnum getPeriodType(){
		return this.TIData.getPeriodType();
	}
	
	public KmeleonPattern getKPattern(){
		return this.KPattern;
	}
	
	public WeekAuthorization getWeekAuthorization(){
		return this.WeekAuth;
	}
	public MonthAuthorization getMonthAuthorization(){
		return this.MonthAuth;
	}
	public YearAuthorization getYearAuthorization(){
		return this.YearAuth;
	}
	public String getMonthPattern() {
		//One day we will need to code this.. but no time ;)
		return null;
	}
	public String getWeekPattern() {
		String Ret="";
	
		if(this.getPeriodType()==PeriodTypeEnum.WEEK){
		

		String[] myDays = this.PosInPeriod.getPositionsPattern().replace(" ","").split(",");
		for(int i=0;i<myDays.length;i++){
			int idx=Integer.valueOf(myDays[i]);
			this.WeekPattern[idx-1]=true;
		}
		
		boolean[] WAuth= this.WeekAuth.getWorkedDays();
		for(int i=0;i<this.WeekPattern.length;i++){
			if(this.WeekPattern[i]==true && WAuth[i]==false){
				this.WeekPattern[i]=false;
			}

			
		}
		
		String AllPos="";
		for(int i=0;i<this.WeekPattern.length;i++){
			if(this.WeekPattern[i]==true){
				int idx=i+1;
				AllPos=AllPos+idx+", ";
			}
		}
		Ret=AllPos.substring(0,AllPos.length()-2);

		}
		else{
			Ret=this.getKPattern().getPositionsInPeriod().getPositionsPattern();
		}
		return Ret;
	}
}
