package com.orsyp.unijob;

import java.util.Calendar;
import java.util.Date;
import com.orsyp.UniverseException;
import com.orsyp.api.Context;
import com.orsyp.api.central.networknode.NetworkNodeId.PRODUCT_TYPE;
import com.orsyp.api.deploymentpackage.PackageId;
import com.orsyp.api.deploymentpackage.PackageItem;
import com.orsyp.api.deploymentpackage.PackageList;
import com.orsyp.std.deploymentpackage.PackageListStdImpl;
import com.orsyp.std.deploymentpackage.PackageStdImpl;

public class UJPackageClear {

	private int DayShift;
	private int HourShift;
	private int MinuteShift;
	private Context context;
	private boolean purgeActive;
	
	public UJPackageClear(boolean purgeActive, int DayShift, int HourShift, int MinuteShift, Context context, String pckSavePath){
		
		this.DayShift=DayShift;
		this.HourShift=HourShift;
		this.MinuteShift=MinuteShift;
		this.context = context;
		this.purgeActive=purgeActive;
	}
	
	public void cleanupPackage() throws UniverseException{
		
		PackageList pckList = new PackageList(context,PRODUCT_TYPE.CENTRAL);
		pckList.setImpl(new PackageListStdImpl());
		pckList.extract();
		Date d2 = getShiftDate(DayShift,HourShift,MinuteShift);
		System.out.println("    +++ Cleaning packages older than: "+d2);
		
		for (int i = 0; i < pckList.getCount();i++){
			PackageItem pck = pckList.get(i);
			Date d1 = pck.getLastModificationDate();
			if (d1.before(d2) && purgeActive){

			PackageId pckID = pck.getIdentifier();
			// Dec 2013 - BSA - Adaptation to UV6 (ex UV4)
			com.orsyp.api.deploymentpackage.Package myPck = new com.orsyp.api.deploymentpackage.Package(pckID);
			myPck.setContext(this.context);
			myPck.setImpl(new PackageStdImpl());
			myPck.extract();
			System.out.println("Deleting Package: "+myPck.getName());
			myPck.delete();
			}
		}
	}
	
	public Date getShiftDate(int Day,int Hour,int Minute){
		Date CurrentDate = new Date();
		Calendar c=Calendar.getInstance();
		c.setTime(CurrentDate);
		c.add(Calendar.DAY_OF_YEAR,-Day);
		c.add(Calendar.HOUR, -Hour);
		c.add(Calendar.MINUTE, -Minute);
		Date d2=c.getTime();
		return d2;
	}
}
