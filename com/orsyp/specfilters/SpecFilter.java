package com.orsyp.specfilters;

import java.util.regex.Pattern;

public class SpecFilter {

	public boolean isActive;
	public String pattern;
	public String fType;
	public String defaultFilter=".*";
	
	public SpecFilter(String pattern){
		this.pattern=pattern;
		isActive=true;
	}
	
	public boolean matchespattern(String s){
		
		// TO DO
		// if last character is *, substitute by .*
		if(isActive){
			if(pattern.equals("")){pattern=defaultFilter;}
		boolean ret = Pattern.matches(pattern, s);
		return ret;}
		else{return true;}
			
	}
	
	public void activateFilter(){
		isActive=true;
	}
	public void deactivateFilter(){
		isActive=false;
	}
	
	public String getPattern(){
		return pattern;
	}
	
	public void setPattern(String t){
		pattern=t;
	}
}
